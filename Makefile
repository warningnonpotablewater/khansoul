.PHONY: all clean docs dist upload

NAME = khansoul

all: clean docs dist

clean:
	rm -f docs/$(NAME).1 docs/$(NAME).5
	rm -rf dist build *.egg-info

docs:
	scdoc < docs/$(NAME).1.scd > docs/$(NAME).1
	scdoc < docs/$(NAME).5.scd > docs/$(NAME).5

dist:
	./setup.py sdist bdist_wheel

upload: all
	twine upload dist/*
