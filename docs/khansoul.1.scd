khansoul(1)

# NAME

khansoul - simple terminal emulator

# SYNOPSIS

khansoul [options...]

# OPTIONS

*-h, --help*
	Show the help message and exit.

*-v, --version*
	Show the program version number and exit.

*-c, FILEPATH --config FILEPATH*
	Use the provided config file instead of the default one.

*-e, COMMAND --execute COMMAND*
	Execute the command on top of the shell.

# AUTHORS

Kirby Kevinson <kirbysemailaddress@protonmail.com>

# SEE ALSO

*khansoul*(5)
