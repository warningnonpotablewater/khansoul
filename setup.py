#!/usr/bin/env python3

import setuptools, khansoul

setuptools.setup(
	name="khansoul",
	version=khansoul.__version__,
	
	packages=setuptools.find_packages(),
	
	data_files=[
		("share/man/man1", ["docs/khansoul.1"]),
		("share/man/man5", ["docs/khansoul.5"]),
		
		("share/applications", ["data/khansoul.desktop"]),
		("share/icons/hicolor/scalable/apps", ["data/khansoul.svg"])
	],
	
	entry_points={
		"console_scripts": [
			"khansoul = khansoul:main"
		]
	},
	
	install_requires=[
		"PyGObject",
		"xdg"
	],
	
	author="Kirby Kevinson",
	author_email="kirbysemailaddress@protonmail.com",
	
	description=khansoul.__description__,
	
	long_description=open("README.md", "r").read(),
	long_description_content_type="text/markdown",
	
	keywords="terminal emulator",
	
	url="https://gitlab.com/kirbykevinson/khansoul",
	
	classifiers=[
		"Development Status :: 5 - Production/Stable",
		"Environment :: X11 Applications :: GTK",
		"License :: OSI Approved :: ISC License (ISCL)",
		"Operating System :: POSIX",
		"Programming Language :: Python :: 3",
		"Topic :: Terminals :: Terminal Emulators/X Terminals"
	]
)
